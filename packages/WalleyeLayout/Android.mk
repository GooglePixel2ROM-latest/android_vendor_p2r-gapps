LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional

LOCAL_SRC_FILES := $(call all-java-files-under, src)

LOCAL_PACKAGE_NAME := WalleyeLayout

LOCAL_SDK_VERSION := current

LOCAL_OVERRIDES_PACKAGES := BullheadLayout
include $(BUILD_PACKAGE)
